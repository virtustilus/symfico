set :application, "perfico"
set :domain,      "--YourServerAddr.Com--"
set :deploy_to,   "/var/www/perfico"
set :app_path,    "app"


set :repository,  "--YOUREPO--"
set :scm,         :git

set :model_manager, "doctrine"

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set  :keep_releases,  3

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL

set :deploy_via, :remote_cache
set :user, "www"
set :branch, "develop"

set :use_composer, true
set :composer_bin, "/usr/local/bin/composer"
set :composer_options,  "--verbose --prefer-source --no-interaction" # Default also contains --no-scripts.

set :dump_assetic_assets, true
set :symfony_env_prod, "prod"
set :cache_warmup, true
set :shared_files,        ["app/config/parameters.yml"]
set :shared_children,     [app_path + "/logs", "web/vendor", "node_modules", "web/upload"]
set :app_config_file, "parameters.yml"

set :use_sudo, false

after "deploy:setup", "dirs:prepare"
after "deploy", "deploy:cleanup"

before "symfony:cache:warmup", "install:assets"
before "install:assets", "install:less"
before "install:assets", "install:bower"
before "install:assets", "doctrine:update"
before "install:assets", "install:js"

#######################################

namespace :dirs do
    task :prepare do
        capifony_pretty_print "--> Preparing directories"

        run "#{try_sudo} sh -c 'cd #{deploy_to} && chown -R #{user}:#{user} *'"
    end
end

namespace :install do
    task :less do
        capifony_pretty_print "--> Install less"

        run "cd #{latest_release} && npm install"
    end
    task :bower do
        capifony_pretty_print "--> Install bower files"

        run "cd #{latest_release} && bower install"
    end
    task :assets do
        capifony_pretty_print "--> Assets:Install"

        run "cd #{latest_release} && #{php_bin} #{symfony_console} assets:install --symlink -e #{symfony_env_prod}"
    end
    task :js do
        capifony_pretty_print "--> JS:Routing"

        run "cd #{latest_release} && #{php_bin} #{symfony_console} fos:js-routing:dump -e #{symfony_env_prod}"
    end
end

namespace :doctrine do
    task :update do
        capifony_pretty_print "--> Doctrine Schema Update"

        run "cd #{latest_release} && #{php_bin} #{symfony_console} doctrine:schema:update --force -e #{symfony_env_prod}"
    end
end

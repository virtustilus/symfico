<?php

namespace Perfico\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PerficoUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}

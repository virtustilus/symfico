<?php

namespace Perfico\UserBundle\DataFixtures\ORM;

use Perfico\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements ContainerAwareInterface
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    public function load(ObjectManager $manager)
    {
        $um = $this->container->get('fos_user.user_manager');

        /** @var User $admin */
        $admin = $um->createUser();
        $admin->setName('Admin');
        $admin->setUsername('admin');
        $admin->setEmail('admin@Perfico.ru');
        $admin->setEnabled(true);
        $admin->setPlainPassword('admin');
        $admin->setRoles(['ROLE_SUPER_ADMIN']);

        $um->updatePassword($admin);
        $um->updateUser($admin, true);
    }

}
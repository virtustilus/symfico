<?php

namespace Perfico\CoreBundle\Form\Handler;

use HWI\Bundle\OAuthBundle\Form\RegistrationFormHandlerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Perfico\CoreBundle\Entity\User;
use Perfico\CoreBundle\Service\Manager\UserManager;

class OAuthRegistrationFormHandler implements RegistrationFormHandlerInterface
{
    protected $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }


    /**
     * Processes the form for a given request.
     *
     * @param Request $request Active request
     * @param Form $form Form to process
     * @param UserResponseInterface $userInformation OAuth response
     *
     * @return boolean True if the processing was successful
     */
    public function process(Request $request, Form $form, UserResponseInterface $userInformation)
    {
        $email = $userInformation->getEmail();

        /* @var $user User */
        $user = $this->userManager->findUserByEmail($email);
        if ($user) {
            $form->setData($user);
            return $user; // just connect to exist user
        }

        // register new user
        /** @var User $user */
        $user = $this->userManager->createUser();

        $user->setEmail($email);

        $user->setUsername($email);
        $user->setEnabled(true);
        $user->setPlainPassword(md5(uniqid()));

        $this->userManager->updateUser($user);

        $form->setData($user);

        return true;
    }
}
<?php

namespace Perfico\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/")
 */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="Perfico_homepage")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }
}
